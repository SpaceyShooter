#All angles are in radians

Class ObjectModel:
	"""The model data for physical and non-physical objects"""
	VertsList = []
	Faces = [] 	#Assumes all triangles!

class PhysicalEntity:
	"""Entities able to effect other entities"""
	VelXYR = [0.0, 0.0, 0.0]
	PosXYR = [0.0, 0.0, 0.0]
	RotXYZ = [0.0, 0.0, 0.0]
	Health = 100
	Model = None	#Expects ObjectModel type

	def render(self):
		pass		

	
class ShipEntity:
	"""Entities able to thrust and must always remain oriented tangent with the sphere"""
	#RotXYZ = tan2sphere(PosXYR)
	
class Zippis:
	PosXYZ = [0.0, 0.0, 0.0]
	Intensity = 1

class GameSphere:
	"""The sphere of which all physical objects are located"""
	Radius = 0.0
	ObjList = []
	Model = None

class BonusItem(PhysicalEntity):
	"""Bonus Items that happen to be floating around"""
	DegradeTime = 20
	DegradesTo  = None

	def Activate(self, player):
		"""Do some function to the player"""
		self.Destroy()
		pass
	def Degrade(self):
		"""Degrade into lesser bonus object"""
		pass
	def Destroy(self):
		"""Remove bonus item from the game"""
		pass

class BonusPoints(BonusItem):
	Value = 0
	def Activate(self, player):
		player.Score += Value
		self.Destroy()
		
class BonusLife(BonusItem):
	def Activate(self, player):
		player.Lives += 1
		self.Destroy()

class BonusShield(BonusItem):
	def Activate(self, player):
		player.ship.Shield = True
		self.Destroy()

class PlayerShip(ShipEntity):
	"""Player's Ship"""
	NumBombs = 0
	NumBoost = 0
	Shield 	 = False
	Weapons  = [] #list of three weapon classes
	

class Player:
	"""Actual player data"""
	Ship 	= None
	Score 	= 0
	Lives 	= 0
	Multi	= 1

class Universe:
	Planet = None
	Stars = []	#List of Zippis
	NebulaImg = None
	MiscJunk = []
	
