import pygame

VERSION = "0.01a"

class RenderWindow:
	renderSurface = None
	queueStack = []

	def __init__(self, size):
		self.renderSurface = pygame.display.set_mode(size, pygame.OPENGL)
		pygame.display.set_caption("Spacey Shooter v%s"%(VERSION))
		pygame.init()

	def processFrame(self):
		#This probably isn't the best way to do things, I'll look into it later.
		for item in queueStack:
			pygame.Surface.blit(item.render(), self.renderSurface)
		pygame.display.flip()
